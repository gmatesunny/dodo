from __future__ import print_function
import time, Queue


def follow(thefile):
    thefile.seek(0, 2)
    while True:
        line = thefile.readline()
        if not line:
            time.sleep(0.1)
            continue
        yield line


def read(q, logfile="/home/roshan/PycharmProjects/Dodo/syslog"):
    with open(logfile, "r") as f:
        loglines = f.readlines()
        for line in loglines:
            q.put_nowait(line)
    with open(logfile, "r") as f:
        loglines = follow(f)
        for line in loglines:
            q.put_nowait(line)

# if __name__ == '__main__':
#     q = Queue.Queue()
#     read(q)
