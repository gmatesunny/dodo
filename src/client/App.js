import React, { Component } from 'react';
import './app.css';
import ReactImage from './react.png';
import SequenceDiagram from 'react-sequence-diagram';

export default class App extends Component {
  state = { time:Date.now(), results: ""};

  componentDidMount() {    
    fetch('/api/getUsername')
      .then(res => res.text()
      .then((text) => {
        this.setState({ results: text });
      })
      )
      this.interval = setInterval(() => this.setState({ time: Date.now()}), 5000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {

    const options = {
      theme: 'simple'
    };

    const input = "" + this.state.results
    return (
      <div>
    <SequenceDiagram input={input} options={options}/>
      </div>
    );
  }
}